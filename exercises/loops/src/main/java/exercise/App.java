package exercise;

class App {
    // BEGIN
    public static void main(String[] args) {
        System.out.println(getAbbreviation( "Portable   Network Graphics"));
    }

    public static String getAbbreviation(String orgString){
        StringBuilder returnBuild = new StringBuilder();
        String[] words = orgString.split(" ");
        for (String word : words) {
            if (!word.equals("")) {
                returnBuild.append(Character.toUpperCase(word.charAt(0)));
            }
        }
        return returnBuild.toString();
    }
    // END
}
