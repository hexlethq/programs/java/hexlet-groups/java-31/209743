package exercise;

import java.util.Arrays;

class App {
    // BEGIN
    public static void main(String[] args) {
        int[] numbers = {3, 10, 4, 3};
        int[] sorted = App.sort(numbers);
        System.out.println(Arrays.toString(sorted)); // => [3, 3, 4, 10]
    }

    public static int[] sort(int[] orgArray) {
        int temp;
        for (int left = 0; left < orgArray.length; left++) {
            int minInd = left;
            for (int i = left; i < orgArray.length; i++) {
                if (orgArray[i] < orgArray[minInd]) {
                    minInd = i;
                }
            }
            temp = orgArray[left];
            orgArray[left] = orgArray[minInd];
            orgArray[minInd] = temp;
        }
        return orgArray;
    }

    // END
}
