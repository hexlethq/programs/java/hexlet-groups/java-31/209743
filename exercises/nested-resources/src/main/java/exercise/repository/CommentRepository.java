package exercise.repository;

import exercise.model.Comment;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.Optional;

@Repository
public interface CommentRepository extends CrudRepository<Comment, Long> {

    // BEGIN
    Optional<Comment> findByIdAndPost_Id(long id, long id1);

    Iterable<Comment> findByPost_Id(long id);

    // END
}
