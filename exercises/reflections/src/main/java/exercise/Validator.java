package exercise;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// BEGIN
public class Validator {
    public static List<String> validate(Address address) {
        List<String> list = new ArrayList<>();
        for (Field field : address.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            NotNull notNull = field.getAnnotation(NotNull.class);
            if (notNull != null) {
                try {
                    if (field.get(address) == null) {
                        list.add(field.getName());
                    }
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
        return list;
    }

    public static Map<String, List<String>> advancedValidate(Address address) {
        Map<String, List<String>> stringListMap = new HashMap<>();
        for (Field field : address.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            Object valueField = null;
            try {
                valueField = field.get(address);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }

            NotNull notNull = field.getAnnotation(NotNull.class);
            if (notNull != null) {
                if (valueField == null) {
                    stringListMap.put(field.getName(), List.of("can not be null"));
                }
            }
            if (valueField != null) {
                MinLength minLength = field.getAnnotation(MinLength.class);
                if (minLength != null) {
                    if (((String) valueField).length() < minLength.minLength()) {
                        stringListMap.put(field.getName(), List.of("length less than 4"));
                    }
                }
            }
        }
        return stringListMap;
    }
}
// END
