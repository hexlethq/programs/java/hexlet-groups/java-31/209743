package exercise;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class App {
    // BEGIN
    public static void main (String[] args){
        int[] numbers1 = {};
        System.out.println(App.getIndexOfMaxNegative(numbers1)); // -1

        int[] numbers2 = {1, 4, 3, 4, 5};
        System.out.println(App.getIndexOfMaxNegative(numbers2)); // -1

        int[] numbers3 = {1, 4, -3, 4, -5};
        System.out.println(App.getIndexOfMaxNegative(numbers3)); // 2

        int[] numbers4 = {1, -3, 5, 4, -3, -10};
        System.out.println(App.getIndexOfMaxNegative(numbers4)); // 1

      /*
         int[] numbers = {1, 2, 6, 3, 8, 12};
        int[] result = App.getElementsLessAverage(numbers);
        System.out.println(Arrays.toString(result)); // => [1, 2, 3]
        int[] numbers5 = {5, 4, 34, 8, 11, -5, 1};
        System.out.println(App.getSumBeforeMinAndMax(numbers5)); // 19
        int[] numbers6 = {7, 1, 37, -5, 11, 8, 1};
         System.out.println(App.getSumBeforeMinAndMax(numbers6)); // 0
      */

    }

    public static int getIndexOfMaxNegative(int[] orgArray) {
        int iNeg = -1;
        if (orgArray.length > 1) {
            int tempNeg = Integer.MIN_VALUE;
            ;
            for (int i = 0; i < orgArray.length; i++) {
                if (orgArray[i] < 0 && tempNeg < orgArray[i]) {
                    tempNeg = orgArray[i];
                    iNeg = i;
                }
            }
        }
        return iNeg;
    }
    public static int [] getElementsLessAverage(int [] orgArray){

        if (orgArray.length > 1) {
            int avergArrays =  0;
            List<Integer> arrlist = new ArrayList<>();

            for (int j : orgArray) avergArrays += j;
            avergArrays /= orgArray.length;
            for (int i = 0; i < orgArray.length; i++) {
                if (orgArray[i] <= avergArrays) {
                    arrlist.add(orgArray[i]);
                }
            }
            int [] returnArray = new int [arrlist.size()];
            for (int i = 0; i < arrlist.size(); i++) {
                returnArray[i] = arrlist.get(i);
            }
            return (returnArray);

        }

        return new int[0];
    }
    public static int  getSumBeforeMinAndMax(int [] orgArray){
        int [] sortArray = orgArray.clone();
       Arrays.sort(sortArray);
       int min = sortArray[0];
       int max = sortArray[sortArray.length-1];
       int indexMax = findValueIndex(orgArray,max);
       int indexMin = findValueIndex(orgArray,min);
       int returnSum = 0;
        for (int i = 0; i < orgArray.length; i++) {
           if((i > indexMax && i < indexMin)|| (i < indexMax && i > indexMin)){
               returnSum =returnSum+orgArray[i];
           }
        }
      return returnSum;

    }
    static int findValueIndex(int[] arr, int value) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == value) {
                return i;
            }
        }
        return -1;
    }
    // END
}
