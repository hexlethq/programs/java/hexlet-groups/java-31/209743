package exercise;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Files;

// BEGIN
public class App {
    public static void save(Path pathIn, Car carIn) throws IOException {
        Files.writeString(pathIn, carIn.serialize());
    }

    public static Car extract(Path pathIn) throws IOException {
        String json = Files.readString(pathIn);
        return Car.unserialize(json);
    }

}
// END
