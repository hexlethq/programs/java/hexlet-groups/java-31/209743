package exercise.controller;

import exercise.model.Course;
import exercise.repository.CourseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@RequestMapping("/courses")
public class CourseController {

    @Autowired
    private CourseRepository courseRepository;

    @GetMapping(path = "")
    public Iterable<Course> getCorses() {
        return courseRepository.findAll();
    }

    @GetMapping(path = "/{id}")
    public Course getCourse(@PathVariable long id) {
        return courseRepository.findById(id);
    }

    // BEGIN
    @GetMapping(path = "/{id}/previous")
    public List<Course> getPreviousCourse(@PathVariable long id) {
        Course course = courseRepository.findById(id);
        List<Long> listCourse;
        if (course.getPath() != null) {
            listCourse = getIdPath(course.getPath());
        } else {
            return List.of();
        }
        return courseRepository.findByIdIn(listCourse);
    }

    private List<Long> getIdPath(String path) {
        char registered = '.';
        if (path.length() > 1) {
            return Arrays.stream(path.split("\\."))
                    .map(Long::parseLong)
                    .collect(Collectors.toList());
        } else {
            return List.of(Long.parseLong(path));
        }

    }
    // END

}
