package exercise;

class Triangle {
    // BEGIN
    public static void main(String[] args){
        System.out.println(getSquare(4,5,45));
    }
    public static double getSquare(double a,double b, double Deg){
        return (a * b / 2 )*  Math.sin(Deg*Math.PI /180);

    }
    // END
}
