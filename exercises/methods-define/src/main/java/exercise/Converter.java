package exercise;

class Converter {
    // BEGIN
    public static void main(String[] args) {

        String text = String.format("%d Kb = %d b",
                10, convert(10, "b"));
        System.out.println(text);
    }

    public static int convert(int Number, String Root) {

        if (Root.equals("b")) {
            return Number * 1024;
        } else if (Root.equals("Kb")) {
            return Number / 1024;
        }
        else return 0;
    }
    // END
}
