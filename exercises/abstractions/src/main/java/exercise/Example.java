package exercise;

public class Example {
    public static void main(String[] args) {

        //слои абстракции
        //customCalculator();

        int[] point = Point.makePoint(3, 4);
        Point.getX(point); // 3
        Point.getY(point); // 4
        System.out.println(Point.pointToString(point)); // "(3, 4)"

        int[] point1 = Point.makePoint(-3, -6);
        int[] symmetricalPoint = Point.getSymmetricalPointByX(point1);
        System.out.println(Point.pointToString(symmetricalPoint)); // (-3, 6)

        int[] point2 = Point.makePoint(0, 0);
        int[] point3 = Point.makePoint(3, 4);
        System.out.println(Point.calculateDistance(point2, point3)); // 5
    }

    /**
     * Вторая реализация метода logResult.
     *
     * @param result Результат, который нужно распечатать.
     */
//    static void logResult(int result) {
//        System.out.println("[CALCULATOR RESULT]: result = " + result);
//    }
    static void logResult(int result) {
        System.out.println("The result of expression = " + result);
    }

    static double division(double a, double b, boolean isReminder) {
        if (isReminder) {
            return a / b;
        } else {
            int aInt = (int) a;
            int bInt = (int) b;
            return aInt / bInt;
        }
    }

    static void customCalculator() {
        // получаем параметры
        int num1 = 1;
        int num2 = 3;
        char symbol = '+';

        int result = calculate(num1, num2, symbol);

        logResult(result);
    }

    static int calculate(int num1, int num2, char symbol) {
        switch (symbol) {
            case '+':
                return num1 + num2;

            case '-':
                return num1 - num2;

            case '*':
                return num1 * num2;

            default:
                return num1 / num2;
        }
    }
}
