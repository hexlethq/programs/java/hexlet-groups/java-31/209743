package exercise;

import java.util.Arrays;

class Point {

    // BEGIN
   public static  int[] makePoint (int x, int y) {
       return  new int[]{x,y};
   }
    public static int getX(int[] point){
        return point[0];
    }
    public static int getY(int[] point){
        return point[1];
    }
    public static String pointToString(int[] point) {
       return "("+getX(point)+", "+getY(point)+")"; //(3, 4)
    }
    public static int getQuadrant(int[] point){
       int x = getX(point);
        int y = getY(point);
       if (x > 0 && y > 0) {
           return  1;
       }
        if (x < 0 && y > 0) {
            return  2;
        }
        if (x < 0 && y < 0) {
            return  3;
        }
        if (x > 0 && y < 0) {
            return  4;
        }
       return  0;
    }
    public static int [] getSymmetricalPointByX (int[] orgPoint){
       return makePoint(getX(orgPoint),-getY(orgPoint));
    }
    public static int calculateDistance(int[] point1,int[] point2){
        int y1 = getY(point1);
        int x1 = getX(point1);
        int y2 = getY(point2);
        int x2 = getX(point2);
        return (int) Math.sqrt(((y2 - y1) * (y2 - y1) + (x2 - x1) * (x2 - x1)));
    }
    // END
}
