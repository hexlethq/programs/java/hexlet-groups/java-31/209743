package exercise;

import java.util.*;
import java.util.stream.Collectors;

// BEGIN
public class App {
    public static Map<String, String> genDiff(Map<String, Object> valOne, Map<String, Object> valTwo) {
        Map<String, String> linkedHashMap = new LinkedHashMap<>();
        for (Map.Entry<String, Object> entry : valOne.entrySet()) {
            if (valTwo.containsKey(entry.getKey())) {
                if (entry.getValue().equals(valTwo.get(entry.getKey()))) {
                    linkedHashMap.put(entry.getKey(), "unchanged");
                } else {
                    linkedHashMap.put(entry.getKey(), "changed");
                }
            } else {
                linkedHashMap.put(entry.getKey(), "deleted");
            }

        }
        for (Map.Entry<String, Object> entryTwo : valTwo.entrySet()) {
            if (!valOne.containsKey(entryTwo.getKey())) {
                linkedHashMap.put(entryTwo.getKey(), "added");
            }
        }

        Map<String, String> linkedHashMapSort = linkedHashMap.entrySet().stream()
                .sorted(Comparator.comparing(Map.Entry::getKey))
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue,
                        (a, b) -> {
                            throw new AssertionError();
                        },
                        LinkedHashMap::new
                ));
        return linkedHashMapSort;

    }
}
//END
