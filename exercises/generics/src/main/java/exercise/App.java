package exercise;

import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import java.util.Objects;

// BEGIN
public class App {
    public static List<Map<String, String>> findWhere(List<Map<String, String>> listBook, Map<String, String> mapFilter) {
        List<Map<String, String>> returnList = new ArrayList<>();
        for (Map<String, String> book : listBook) {
            int count = 0;
            for (Map.Entry<String, String> entryFilter : mapFilter.entrySet()) {
                if (book.containsKey(entryFilter.getKey())) {
                    if (Objects.equals(book.get(entryFilter.getKey()), entryFilter.getValue())) {
                        count += 1;
                    }
                    if (count == mapFilter.size()) {
                        returnList.add(book);
                    }
                }
            }
        }
        return returnList;
    }
}
//END

