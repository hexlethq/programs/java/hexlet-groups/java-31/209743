package exercise;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;


class ReversedSequenceTest {

    ReversedSequence text;

    @BeforeEach
    void prep() {
        text = new ReversedSequence("abcdef");
    }

    @Test
    void toStringTest() {
        String expected = "fedcba";
        String result = text.toString();

        assertThat(result).isEqualTo(expected);
    }

    @Test
    void length() {
        int expected = 6;
        int result = text.length();

        assertThat(result).isEqualTo(expected);

    }

    @Test
    void charAt() {
        char expected = 'e';
        char result = text.charAt(1);

        assertThat(result).isEqualTo(expected);
    }

    @Test
    void subSequence() {
        String expected = "edc";
        String result = text.subSequence(1, 4).toString();
        assertThat(result).isEqualTo(expected);
    }
}