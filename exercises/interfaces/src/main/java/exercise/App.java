package exercise;

import java.util.Comparator;
import java.util.List;

// BEGIN
public class App {
    public static List<String> buildAppartmentsList(List<Home> homeList, int count) {
        homeList.sort(App.COMPARE_BY_AREA);
        return homeList.stream()
                .limit(count)
                .map((s) -> s.toString())
                .toList();
    }

    public static final Comparator<Home> COMPARE_BY_AREA = Home::compareTo;

}
// END
