package exercise;

import java.util.List;
import java.util.Arrays;

// BEGIN
public class App {
    public static long getCountOfFreeEmails(List<String> listIn) {
        long numGmail = listIn.stream()
                .filter(str -> str.contains("@gmail.com"))
                .count();
        long numYandex = listIn.stream()
                .filter(str -> str.contains("@yandex.ru"))
                .count();
        long numHotmail = listIn.stream()
                .filter(str -> str.contains("@hotmail.com"))
                .count();

        return numGmail + numYandex + numHotmail;
    }
}
// END

