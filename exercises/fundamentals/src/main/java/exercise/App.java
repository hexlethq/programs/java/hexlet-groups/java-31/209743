package exercise;

class App {
    public static void numbers() {
        // BEGIN
        System.out.println(8/2 + 100%3);
        // END
    }

    public static void strings() {
        String language = "Java";
        String str1 = " works ";
        String str2 = "on ";
        String str3 = "JVM";
        // BEGIN
        System.out.println(language+str1+str2+str3);
        // END
    }

    public static void converting() {
        Number soldiersCount = 300;
        String name = "spartans";
        // BEGIN
        System.out.println(soldiersCount +" "+name);
        // END
    }
}
