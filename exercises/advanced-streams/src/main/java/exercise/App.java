package exercise;

import java.util.List;
import java.util.stream.Collectors;
import java.util.Arrays;
import java.util.stream.Stream;

// BEGIN
public class App {
    public static String getForwardedVariables(String stringIn) {

        Stream<String[]> arrStringfilt = Arrays.stream(stringIn.split("\n"))
                .filter(str -> str.startsWith("environment="))
                .map(str -> str.replaceAll("\"", "").trim())
                .map(str -> str.replaceAll("environment=", ""))
                .map(str -> str.split(","));


        List<String> listStr = arrStringfilt
                .map(s -> Stream.of(s))
                .flatMap(s -> s.filter(s1 -> s1.startsWith("X_FORWARDED_")))
                .map(str -> str.replaceAll("X_FORWARDED_", ""))
                .collect(Collectors.toList());

        String strReturn = "";
        for (String str : listStr) {
            if ((listStr.size() - 1) != listStr.indexOf(str)) {
                strReturn = strReturn + str + ",";
            } else {
                strReturn = strReturn + str;
            }
        }

        return strReturn;
    }

}
//END

