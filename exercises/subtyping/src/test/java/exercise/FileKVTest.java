package exercise;

import java.util.HashMap;

import org.junit.jupiter.api.BeforeEach;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
// BEGIN

// END


class FileKVTest {

    private static Path filepath = Paths.get("src/test/resources/file").toAbsolutePath().normalize();

    @BeforeEach
    public void beforeEach() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        String content = mapper.writeValueAsString(new HashMap<String, String>());
        Files.writeString(filepath, content);
    }

    // BEGIN
    @Test
    void FileKVTestNormalWork() {
        KeyValueStorage storage = new FileKV("src/test/resources/file", Map.of("key", "10"));
        assertThat(storage.get("key2", "default")).isEqualTo("default");
        assertThat(storage.get("key", "default")).isEqualTo("10");

        storage.set("key2", "value2");
        storage.set("key", "value");

        assertThat(storage.get("key2", "default")).isEqualTo("value2");
        assertThat(storage.get("key", "default")).isEqualTo("value");

        storage.unset("key");
        assertThat(storage.get("key", "def")).isEqualTo("def");
        assertThat(storage.toMap()).isEqualTo(Map.of("key2", "value2"));

    }
    @Test
    void FileKVInitTest(){
        KeyValueStorage storage = new FileKV("src/test/resources/file", Map.of("key", "value"));
        String expect = "{\"key\":\"value\"}";
        assertThat(Utils.readFile("src/test/resources/file")).isEqualTo(expect);
    }
    @Test
    void FileKVSetTest(){
        KeyValueStorage storage = new FileKV("src/test/resources/file", Map.of("key", "10"));
        storage.set("key2", "value2");
        storage.set("key", "value");

        String expect = "{\"key\":\"value\",\"key2\":\"value2\"}";
        assertThat(Utils.readFile("src/test/resources/file")).isEqualTo(expect);
    }
    @Test
    void FileKVUnSetTest(){
        KeyValueStorage storage = new FileKV("src/test/resources/file", new HashMap<>());
        storage.set("key2", "value2");
        storage.set("key", "value");

        storage.unset("key");

        String expect = "{\"key2\":\"value2\"}";
        assertThat(Utils.readFile("src/test/resources/file")).isEqualTo(expect);
    }

    @Test
    void FileKVTestGetTest() {
        KeyValueStorage storage = new FileKV("src/test/resources/file", Map.of("key", "10"));
        assertThat(storage.get("key2", "default")).isEqualTo("default");
        assertThat(storage.get("key", "default")).isEqualTo("10");

        storage.set("key2", "value2");
        storage.set("key", "value");

        assertThat(storage.get("key2", "default")).isEqualTo("value2");
    }
    // END
}
