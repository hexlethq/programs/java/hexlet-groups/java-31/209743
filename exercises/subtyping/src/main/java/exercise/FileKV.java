package exercise;

import java.util.HashMap;
import java.util.Map;

// BEGIN
public class FileKV implements KeyValueStorage {

    private final String path;

    public FileKV(String path, Map<String, String> mapIn) {
        Map<String, String> map = new HashMap<>(mapIn);
        this.path = path;
        String json = Utils.serialize(map);
        Utils.writeFile(this.path, json);
    }

    @Override
    public void set(String key, String value) {
        Map<String, String> map = getMapToFile();
        map.put(key, value);

        String json = Utils.serialize(map);
        Utils.writeFile(this.path, json);
    }

    @Override
    public void unset(String key) {
        Map<String, String> map = getMapToFile();
        map.remove(key);

        String json = Utils.serialize(map);
        Utils.writeFile(this.path, json);
    }

    @Override
    public String get(String key, String defaultValue) {
        Map<String, String> map = getMapToFile();
        if (map.containsKey(key)) {
            return map.get(key);
        }
        return defaultValue;
    }


    @Override
    public Map<String, String> toMap() {
        return new HashMap<>(getMapToFile());
    }

    private Map<String, String> getMapToFile() {

        String json = Utils.readFile(this.path);
        return Utils.unserialize(json);

    }
}
// END
