package exercise;

import java.util.HashMap;
import java.util.Map.Entry;

// BEGIN
public class App {
    public static void swapKeyValue(KeyValueStorage valueStorage) {

        HashMap<String, String> hashMap = new HashMap<>();
        for (Entry<String, String> entry : valueStorage.toMap().entrySet()) {
            hashMap.put(entry.getValue(), entry.getKey());
            valueStorage.unset(entry.getKey());
        }
        for (Entry<String, String> entry :hashMap.entrySet()){
            valueStorage.set(entry.getKey(),entry.getValue());
        }
    }
}
// END
