package exercise;

import java.time.LocalDateTime;

import exercise.daytimes.Daytime;
import exercise.daytimes.Morning;
import exercise.daytimes.Day;
import exercise.daytimes.Evening;
import exercise.daytimes.Night;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

// BEGIN
// Аннотация @Configuration указывает, что класс содержит методы, создающие бины
@Configuration
public class MyApplicationConfig {

    // Аннотация @Bean указывает, что метод возвращает бин, который нужно добавить в контекст
    // Spring будет вызывать методы этого класса, помеченные аннотацией @Bean
    // и полученные объекты добавлять в контекст
    @Bean
    public Daytime getNowDaytime() {
        int hour = LocalDateTime.now().getHour();
        if (hour >= 6 && hour < 12) {
            return new Morning();
        } else if (hour >= 12 && hour < 18) {
            return new Day();
        } else if (hour >= 18 && hour < 23) {
            return new Evening();
        } else if (hour >= 23) {
            return new Night();
        } else if (hour < 6) {
            return new Night();
        }
        return new Night();
    }
}
// END
