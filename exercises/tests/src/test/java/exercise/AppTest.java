package exercise;

import java.util.List;
import java.util.Arrays;
import java.util.ArrayList;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class AppTest {

    List<Integer> numbers;

    @BeforeEach
    void initNumbers() {
        this.numbers = new ArrayList<>();
        this.numbers.add(1);
        this.numbers.add(2);
        this.numbers.add(3);
        this.numbers.add(4);
    }


    @Test
    void testTake() {
        // BEGIN
        List<Integer> returnLis = App.take(this.numbers, 2);
        List<Integer> test =  new ArrayList<>(Arrays.asList(1, 2));
        Assertions.assertArrayEquals(returnLis.toArray(),test.toArray());
        // END
    }
}
