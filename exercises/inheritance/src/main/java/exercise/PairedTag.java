package exercise;

import java.util.Map;
import java.util.List;

// BEGIN
public class PairedTag extends Tag {
    private final String body;
    private final List<Tag> children;

    public PairedTag(String name, Map<String, String> attributes, String body, List<Tag> children) {
        super(name, attributes);
        this.body = body;
        this.children = children;
    }

    @Override
    public String toString() {
        StringBuilder returnString = new StringBuilder(super.toString());
        returnString.append(this.body);
        for (Tag tag : children) {
            returnString.append(tag.toString());
        }
        returnString.append("</").append(super.name).append(">");
        return returnString.toString();
    }
}
// END
