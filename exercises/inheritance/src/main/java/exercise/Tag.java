package exercise;

import java.util.Map;

// BEGIN
public class Tag {
    protected String name;
    protected Map<String, String> attributes;


    public Tag(String name, Map<String, String> attributes) {
        this.name = name;
        this.attributes = attributes;
    }

    @Override
    public String toString() {
        StringBuilder returnString = new StringBuilder("<" + this.name);
        for (Map.Entry<String, String> entry : attributes.entrySet()) {
            returnString.append(" ").append(entry.getKey()).append("=\"").append(entry.getValue()).append("\"");
        }
        returnString.append(">");
        return returnString.toString();
    }


}
// END
