package exercise;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

// BEGIN
public class App {

    public static boolean scrabble(String inPartOfWord, String inCheckWord) {

        List<String> list;
        List<String> checkWordList;
        String partOfWord = inPartOfWord.toLowerCase();
        String checkWord = inCheckWord.toLowerCase();

        list = new ArrayList(Arrays.asList(partOfWord.split("")));
        checkWordList = new ArrayList(Arrays.asList(checkWord.split("")));

        if (checkWord.length() > list.size()) {
            return false;
        }

        for (String strCheck : checkWordList) {
            boolean check = list.remove(strCheck);
            if (!check) {
                return false;
            }
        }

        return true;
    }
}
//END

