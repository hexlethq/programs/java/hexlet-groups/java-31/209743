package exercise;

class App {
    // BEGIN
    public static void main(String[] args) {
        System.out.println(App.getFinalGrade((short)100, (short)12)); // 100
        System.out.println(App.getFinalGrade((short)99, (short)0)); // 100
        System.out.println(App.getFinalGrade((short)10, (short)15)); // 100
        System.out.println(App.getFinalGrade((short)85, (short)5)); // 90
        System.out.println(App.getFinalGrade((short)55, (short)3)); // 75
        System.out.println(App.getFinalGrade((short)55, (short)0)); // 0
    }



    public static short getFinalGrade(short exam,
                                      short project){
        if (exam >90 || project > 10){
            return 100;
        }
        else if (exam >75 && project >= 5){
            return 90;
        }
        else if (exam >50 && project >= 2){
            return 75;
        }
        return  0;

    }
    public static String getTypeOfTriangle(double a, double b ,double c){
        if ((a+b <= c) || (a+c <= b) || (b+c <= a)){
           return "Треугольник не существует";
        }
        else if (a != b && a != c && b != c) {
            return"Разносторонний";
        }
        else if   (a == b && b == c) {
            return"Равносторонний";
        }
        else {
            return"Равнобедренный";
        }
    }
    // END
}
