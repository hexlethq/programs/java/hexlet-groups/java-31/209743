package exercise.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.List;
import java.util.Map;

import java.nio.file.Paths;
import java.nio.file.Path;
import java.nio.file.Files;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.commons.lang3.ArrayUtils;

public class UsersServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws IOException, ServletException {

        String pathInfo = request.getPathInfo();

        if (pathInfo == null) {
            showUsers(request, response);
            return;
        }

        String[] pathParts = pathInfo.split("/");
        String id = ArrayUtils.get(pathParts, 1, "");

        showUser(request, response, id);
    }

    private List getUsers() throws IOException {
        // BEGIN
        ObjectMapper objectMapper = new ObjectMapper();
        String json = new String(Files.readAllBytes(Paths.get("src/main/resources/users.json")));
        List<Map<String, String>> list = objectMapper.readValue(json, new TypeReference<List<Map<String, String>>>() {
        });
        return list;
        // END
    }

    private void showUsers(HttpServletRequest request,
                           HttpServletResponse response)
            throws IOException {

        // BEGIN
        List<Map<String, String>> listUsers = getUsers();
        StringBuilder body = new StringBuilder();

        body.append("""
                <!DOCTYPE html>
                <html lang="ru">
                    <head>
                        <!-- Required meta tags -->
                        <meta charset="utf-8">
                        <meta name="viewport" content="width=device-width, initial-scale=1">
                                              
                        <!-- Bootstrap CSS -->
                        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
                                              
                        <title>Example application | Users</title>
                    </head>
                    <body>
                    <table>
                """);

        for (Map<String, String> map : listUsers) {
            body.append("<tr>");
            body.append("<td>").append(map.get("id")).append("</td>");

            body.append("<td>")
                    .append("<a href=\"/users/").append(map.get("id")).append("\">")
                    .append(map.get("firstName")).append(" ").append(map.get("lastName")).append("</a>")
                    .append("</td>");
            body.append("</tr>");
        }

        body.append("""
                    </table>
                    </body>
                </html>
                """);
        response.setContentType("text/html;charset=UTF-8");

        PrintWriter out = response.getWriter();
        out.println(body.toString());
        // END
    }

    private void showUser(HttpServletRequest request,
                          HttpServletResponse response,
                          String id)
            throws IOException {

        // BEGIN
        List<Map<String, String>> listUsers = getUsers();
        Map<String, String> map = listUsers.stream().filter(x -> x.get("id").equals(id)).findFirst().orElse(null);
        if (map == null) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }

        // Создаем html разметку при помощи String builder и подключаем стили
        StringBuilder body = new StringBuilder();
        body.append("""
                <!DOCTYPE html>
                <html lang=\"ru\">
                    <head>
                        <meta charset=\"UTF-8\">
                        <title>Example application | Users</title>
                        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
                          
                    </head>
                    <body>
                """);
        body.append("<tr>");
        body.append("<td>").append(map.get("id")).append("</td>");

        body.append("<td>")
                .append("<a href=\"/users/").append(map.get("id")).append("\">")
                .append(map.get("firstName")).append(" ").append(map.get("lastName")).append("</a>")
                .append("</td>");

        body.append("<td>").append(map.get("email")).append("</td>");
        body.append("</tr>");

        body.append("""
                    </body>
                </html>
                """);

        // Устанавливаем тип содержимого ответа и кодировку
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        out.println(body.toString());
        // END
    }
}
