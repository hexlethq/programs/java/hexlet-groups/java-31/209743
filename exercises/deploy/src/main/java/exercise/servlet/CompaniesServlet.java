package exercise.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.List;
import java.util.stream.Collectors;

import static exercise.Data.getCompanies;

public class CompaniesServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws IOException, ServletException {

        // BEGIN
        List<String> companies = getCompanies();
        PrintWriter out = response.getWriter();

        String search = request.getParameter("search");

        if (search == null || search.isEmpty()) {
            out.println(companies.stream().collect(Collectors.joining("\n")));
        } else {
            String strOut = companies.stream().filter(x -> x.contains(search)).collect(Collectors.joining("\n"));
            if (strOut.isEmpty()) {
                out.println("Companies not found");
            } else {
                out.println(strOut);
            }
        }
        // END
    }
}
