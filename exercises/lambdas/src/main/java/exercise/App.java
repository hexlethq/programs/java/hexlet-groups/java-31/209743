package exercise;

import java.util.Arrays;
import java.util.stream.Stream;

// BEGIN
public class App {
    public static String[][] enlargeArrayImage(String[][] arrIN) {

        return Arrays.stream(arrIN)
                .flatMap(s -> Stream.of(s, s))
                .map(str2 -> Arrays.stream(str2).flatMap(s -> Stream.of(s, s)))
                .map(s -> s.toArray(String[]::new))
                .toArray(String[][]::new);
    }
}
// END

