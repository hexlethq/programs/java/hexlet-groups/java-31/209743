package exercise;

import java.util.Comparator;
import java.util.Map;
import java.util.List;
import java.util.stream.Collectors;

// BEGIN
public class Sorter {
    public static List<String> takeOldestMans (List<Map<String, String>> listIn){
        return listIn.stream()
                .filter(mp-> mp.get("gender").equals("male"))
                .sorted(Comparator.comparing(o -> o.get("birthday")))
                .map(mp-> mp.get("name"))
                .collect(Collectors.toList());
    }
}

// END
