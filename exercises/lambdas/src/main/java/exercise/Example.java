package exercise;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class Example {

    public static void main(String[] par) {

//        String[][] image = new String[][]{
////                {"*", "*", "*", "*"},
////                {"*", " ", " ", "*"},
////                {"*", " ", " ", "*"},
////                {"*", "*", "*", "*"},
////        };
//                Stream < String > streamOfArray = Arrays.stream(image);
//        streamOfArray.flatMap(s->Stream.of(s,s)) //Преобразование слова в массив букв
//                //.flatMap(Arrays::stream).distinct() //выравнивает каждый сгенерированный поток в один поток
//                .collect(Collectors.toList()).forEach(System.out::println);

//        String[][] image = {
//                {"*", "*", "*", "*"},
//                {"*", " ", " ", "*"},
//                {"*", " ", " ", "*"},
//                {"*", "*", "*", "*"},
//        };
//
//        System.out.println(Arrays.deepToString(image)); // =>
//// [
////     [*, *, *, *],
////     [*,  ,  , *],
////     [*,  ,  , *],
////     [*, *, *, *],
//// ]
//
//        String[][] enlargedImage = App.enlargeArrayImage(image);
//        System.out.println(Arrays.deepToString(enlargedImage)); // =>

// [
//     [*, *, *, *, *, *, *, *],
//     [*, *, *, *, *, *, *, *],
//     [*, *,  ,  ,  ,  , *, *],
//     [*, *,  ,  ,  ,  , *, *],
//     [*, *,  ,  ,  ,  , *, *],
//     [*, *,  ,  ,  ,  , *, *],
//     [*, *, *, *, *, *, *, *],
//     [*, *, *, *, *, *, *, *],
// ]

        Map<String, String> user = Map.of(
                "name", "Andrey Belov",
                "birthday", "1980-11-23",
                "gender", "male"
        );
        System.out.println(user); // => {name=Andrey Belov, gender=male, birthday=1980-11-23}

        List<Map<String, String>> users = List.of(
                Map.of("name", "Vladimir Nikolaev", "birthday", "1990-12-27", "gender", "male"),
                Map.of("name", "Andrey Petrov", "birthday", "1989-11-23", "gender", "male"),
                Map.of("name", "Anna Sidorova", "birthday", "1996-09-09", "gender", "female"),
                Map.of("name", "John Smith", "birthday", "1989-03-11", "gender", "male"),
                Map.of("name", "Vanessa Vulf", "birthday", "1985-11-16", "gender", "female"),
                Map.of("name", "Alice Lucas", "birthday", "1986-01-01", "gender", "female"),
                Map.of("name", "Elsa Oscar", "birthday", "1970-03-10", "gender", "female")
        );


        List<String> men = Sorter.takeOldestMans(users);
        System.out.println(men); // ["John Smith", "Andrey Petrov", "Vladimir Nikolaev"]

    }
}
