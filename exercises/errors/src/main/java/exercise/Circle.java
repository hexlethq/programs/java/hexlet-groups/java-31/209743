package exercise;

// BEGIN
public class Circle {
    private final Point point;
    private final int rad;

    public Circle(Point point, int rad) {
        this.point = point;
        this.rad = rad;
    }

    public int getRadius() {
        return rad;
    }

    public double getSquare() throws NegativeRadiusException {
        if (rad > 0 || rad == 0) {
            return Math.PI * (rad * rad);
        } else {
            throw new NegativeRadiusException("Радиус меньше нуля");
        }
    }
}
// END
