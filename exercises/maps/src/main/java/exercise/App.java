package exercise;

import java.util.HashMap;
import java.util.Map;

// BEGIN
public class App {
    public static Map getWordCount(String text) {
        Map<String, Integer> returnMap = new HashMap<>();
        text.trim();
        if (text.length() != 0) {
            String[] words = text.split(" ");
            for (String word : words) {
                if (returnMap.containsKey(word)) {
                    Integer oldVa = returnMap.get(word);
                    returnMap.put(word, oldVa + 1);
                } else {
                    returnMap.put(word, 1);
                }
            }
        }
        return returnMap;
    }

    public static String toString(Map<String, Integer> inMap) {
        String returnString = "";
        if (inMap.isEmpty()) {
            return "{}";
        }
        returnString = returnString + "{\n";
        for (Map.Entry<String, Integer> student : inMap.entrySet()) {
            returnString = returnString +
                    "  " + student.getKey() + ": " + student.getValue() + "\n";
        }
        returnString = returnString + "}\n";
        return returnString;
    }
}
//END

