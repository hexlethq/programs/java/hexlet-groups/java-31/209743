package exercise.controllers;

import io.javalin.http.Context;
import io.javalin.apibuilder.CrudHandler;
import io.ebean.DB;

import java.util.List;

import exercise.domain.User;
import exercise.domain.query.QUser;

public class UserController implements CrudHandler {

    public void getAll(Context ctx) {
        // BEGIN
        List<User> users = new QUser()
                .orderBy()
                .id.asc()
                .findList();

        String json = DB.json().toJson(users);
        ctx.json(json);
        // END
    }

    ;

    public void getOne(Context ctx, String id) {

        // BEGIN
        User userone = new QUser()
                .id.equalTo(Integer.parseInt(id))
                .findOne();

        String json = DB.json().toJson(userone);
        ctx.json(json);
        // END
    }

    ;

    public void create(Context ctx) {

        // BEGIN

        User user = ctx.bodyValidator(User.class)
                .check(obj -> obj.getEmail() != null, "Email not null")
                .check(obj -> obj.getFirstName() != null, "FirstName not null")
                .check(obj -> obj.getLastName() != null, "LastName not null")
                .get();
        user.save();
        // END
    }

    ;

    public void update(Context ctx, String id) {
        // BEGIN

        User userIn = ctx.bodyValidator(User.class)
                .check(obj -> obj.getEmail() != null, "Email not null")
                .check(obj -> obj.getFirstName() != null, "FirstName not null")
                .check(obj -> obj.getLastName() != null, "LastName not null")
                .get();
        new QUser()
                .id.equalTo(Integer.parseInt(id))
                .asUpdate() // делает из запроса update-запрос
                .set("firstName", userIn.getFirstName())
                .set("lastName", userIn.getLastName())
                .set("email", userIn.getEmail())
                .set("password", userIn.getPassword())
                .update();
        // END
    }

    ;

    public void delete(Context ctx, String id) {
        // BEGIN
        new QUser()
                .id.equalTo(Integer.parseInt(id))
                .delete();
        // END
    }

    ;
}
