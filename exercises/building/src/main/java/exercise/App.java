// BEGIN

package exercise;

import com.google.gson.*;

public class App {
    public static void main(String[] args) {
        System.out.println("Hello, World!");
    }

    public static String toJson(String[] arrayIn) {
     
        Gson gson = new Gson();
        String json = gson.toJson(arrayIn);
        return json;
    }
}
// END
