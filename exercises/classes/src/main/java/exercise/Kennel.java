package exercise;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


// BEGIN
class Kennel {
    public static List<String[]> Puppys = new ArrayList<String[]>();

    public static void addPuppy(String[] puppy) {
        Kennel.Puppys.add(puppy);
    }

    public static void addSomePuppies(String[][] lotPuppy) {
        for (String[] puppy : lotPuppy) {
            addPuppy(puppy);
        }
    }

    public static int getPuppyCount() {
        return Kennel.Puppys.size();
    }

    public static boolean isContainPuppy(String name) {
        for (String[] pup : Kennel.Puppys) {
            if (pup[0].equals(name)) {
                return true;
            }
        }
        return false;
    }

    public static String[][] getAllPuppies() {
        return Kennel.Puppys.toArray(new String[0][0]);
    }

    public static String[] getNamesByBreed(String Breed) {
        List<String> listReturn = new ArrayList<>();
        for (String[] pup : Kennel.Puppys) {
            if (pup[1].equals(Breed)) {
                listReturn.add(pup[0]);
            }
        }
        return listReturn.toArray(new String[0]);
    }

    public static void resetKennel() {
        Kennel.Puppys = new ArrayList<>();
    }

    public static boolean removePuppy(String name) {
        Iterator<String[]> puppysIterator = Kennel.Puppys.iterator();
        while (puppysIterator.hasNext()) {
            String[] nextPup = puppysIterator.next();
            if (nextPup[0].equals(name)) {
                puppysIterator.remove();
                return true;
            }
        }
        return false;
    }
}
// END
