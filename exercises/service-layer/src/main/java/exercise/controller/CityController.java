package exercise.controller;

import exercise.CityNotFoundException;
import exercise.model.City;
import exercise.repository.CityRepository;
import exercise.service.WeatherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@RestController
public class CityController {

    @Autowired
    private CityRepository cityRepository;

    @Autowired
    private WeatherService weatherService;

    // BEGIN
    @GetMapping(path = "/cities/{id}")
    public Map<String, String> getCityWeather(@PathVariable long id) {

        City city = cityRepository.findById(id).orElseThrow(() -> new CityNotFoundException("City not Found"));

        return weatherService.getWeatherByCityName(city.getName());
    }

    @GetMapping(path = "/search")
    public List<Map<String, String>> getCityWeatherByNameOrAll(@RequestParam(required = false) String name) {

        List<City> cityList;
        if (name == null) {
            cityList = cityRepository.findByOrderByNameAsc();

        } else {
            cityList = cityRepository.findByNameStartingWith(name);
        }

        return cityList.stream()
                .map(city -> weatherService.getTempAndCityName(city.getName()))
                .collect(Collectors.toList());

    }

    // END
}

