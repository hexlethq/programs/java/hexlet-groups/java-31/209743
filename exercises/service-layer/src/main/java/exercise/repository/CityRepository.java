package exercise.repository;

import exercise.model.City;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CityRepository extends CrudRepository<City, Long> {


    // BEGIN

    @Query("select c from City c where  UPPER(c.name) like  UPPER(concat(?1, '%')) ORDER BY c.name")
    List<City> findByNameStartingWith(String name);

    List<City> findByOrderByNameAsc();

    // END
}
