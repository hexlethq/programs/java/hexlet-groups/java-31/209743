package exercise;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;

import java.lang.reflect.Proxy;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

// BEGIN
@Component
public class CustomBeanPostProcessor implements BeanPostProcessor {

    public static Logger LOGER = LoggerFactory.getLogger(CustomBeanPostProcessor.class);

    private static Map<String, String> mapBean = new HashMap<>();

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {

        if (bean.getClass().isAnnotationPresent(Inspect.class)) {
            mapBean.put(beanName, bean.getClass().getAnnotation(Inspect.class).level());
        }

        return BeanPostProcessor.super.postProcessBeforeInitialization(bean, beanName);
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        if (mapBean.get(beanName) != null) {
            Object proxyInstance = Proxy.newProxyInstance(
                    bean.getClass().getClassLoader(),
                    bean.getClass().getInterfaces(),
                    // Лямбда - обработчик вызова
                    // В качестве аргумента принимает сам объект прокси, метод, к которому происходит обращение
                    // и массив аргументов, переданных при вызове
                    (proxy, method, args) -> {
                        String message = "Was called method: " + method.getName() + "() with arguments: " + Arrays.toString(args);
                        if (mapBean.get(beanName).equals("debug")) {
                            LOGER.debug(message);
                        } else {
                            LOGER.info(message);
                        }
                        return method.invoke(bean, args);

                    });
            return BeanPostProcessor.super.postProcessAfterInitialization(proxyInstance, beanName);
        }
        return BeanPostProcessor.super.postProcessAfterInitialization(bean, beanName);
    }
}
// END
