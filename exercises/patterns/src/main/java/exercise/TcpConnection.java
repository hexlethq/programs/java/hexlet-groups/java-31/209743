package exercise;

import exercise.connections.Connection;
import exercise.connections.Disconnected;

// BEGIN
public class TcpConnection {

    private final String ip;
    private final int port;

    private Connection connection;


    public TcpConnection(String ip, int port) {
        this.ip = ip;
        this.port = port;
        this.connection = new Disconnected(this);
    }

    public String getCurrentState() {
        return connection.toString();
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    public void connect(){
        this.connection.connect();
    }

    public void disconnect(){
        this.connection.disconnect();
    }
    public void write(String data){
        this.connection.write(data);
    }
}
// END
