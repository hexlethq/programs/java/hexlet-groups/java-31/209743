package exercise;

// BEGIN
public class Segment {
    private Point BeginPoint;
    private Point EndPoint;

    public Segment(Point BeginPoint, Point EndPoint) {
        this.BeginPoint = BeginPoint;
        this.EndPoint = EndPoint;
    }

    public Point getBeginPoint() {
        return BeginPoint;
    }

    public Point getEndPoint() {
        return EndPoint;
    }

    public Point getMidPoint() {
        int xMidPoint = (BeginPoint.getX() + EndPoint.getX()) / 2;
        int yMidPoint = (BeginPoint.getY() + EndPoint.getY()) / 2;

        return new Point(xMidPoint, yMidPoint);
    }
}
// END
