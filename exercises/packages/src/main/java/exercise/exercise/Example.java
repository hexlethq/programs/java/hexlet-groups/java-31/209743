package exercise;


import exercise.geometry.Point;
import exercise.geometry.Segment;
import exercise.stack.Stack;


import java.util.Arrays;


public class Example {
    public static void main(String[] args) {

        double[] point1 = Point.makePoint(2, 3);
        double[] point2 = Point.makePoint(4, 5);
        double[][] segment = Segment.makeSegment(point1, point2);
        double[] beginPoint = Segment.getBeginPoint(segment);
        double[] endPoint = Segment.getEndPoint(segment);
        System.out.println(Arrays.toString(beginPoint)); // => [2, 3]
        System.out.println(Arrays.toString(endPoint)); // => [4, 5]
        double[] point11 = Point.makePoint(3, 4);
        double[] point22 = Point.makePoint(6, 7);
        double[][] segment1 = Segment.makeSegment(point11, point22);

        double[] midPoint = App.getMidpointOfSegment(segment1);
        System.out.println(Arrays.toString(midPoint)); // => [4.5, 5.5]

        double[][] reversedSegment = App.reverse(segment1);
        double[] beginPoint1 = Segment.getBeginPoint(reversedSegment);
        double[] endPoint1 = Segment.getEndPoint(reversedSegment);
        System.out.println(Arrays.toString(beginPoint1)); // => [6, 7]
        System.out.println(Arrays.toString(endPoint1)); // => [3, 4]


        double[][] segment11 = Segment.makeSegment(Point.makePoint(1, 4), Point.makePoint(5, 8));
        System.out.println(App.isBelongToOneQuadrant(segment11)); // true

        double[][] segment2 = Segment.makeSegment(Point.makePoint(1, 4), Point.makePoint(-2, 8));
        System.out.println(App.isBelongToOneQuadrant(segment2)); // false

        double[][] segment3 = Segment.makeSegment(Point.makePoint(1, 4), Point.makePoint(0, 0));
        System.out.println(App.isBelongToOneQuadrant(segment3)); // false
//        // пакеты
////        // получили пользователя по сети
//        org.extra.hw.hexlet.pr14.request.User inputUser = new org.extra.hw.hexlet.pr14.request.User();
//        inputUser.setId(1);
//        inputUser.setEmail("egor@yakovlev.ru");
//
//        org.extra.hw.hexlet.pr14.request.Password password = new org.extra.hw.hexlet.pr14.request.Password();
//        password.setPassword("qwerty1234");
//        password.setConfirmation("qwert1234");
//
//        inputUser.setPassword(password);
//        inputUser.setUsername("EgorYakovlev");
//        System.out.println(inputUser);
////
//        //сохранили нового пользователя в БД и вернули "сокращенную" версию пользователя
//        org.extra.hw.hexlet.pr14.response.User outputUser = new org.extra.hw.hexlet.pr14.response.User(
//            "EgorYakovlev", "egor@yakovlev.ru"
//        );
//        System.out.println(outputUser);
//
//        System.out.println(Objects.equals(inputUser, outputUser));

        // import

//        User inputUser = new User();
    //    org.extra.hw.hexlet.pr14.response.User outputUser = new org.extra.hw.hexlet.pr14.response.User(
    //        "", "egor@yakovlev.ru"
    //    );


        // задача с собеседования о стеке - проверить, является ли скобочная последовательность правильной
//        String example = "{}[)";
//
//        if (check(example) == 0) {
//            System.out.println("Правильная скобочная последовательность");
//        } else {
//            System.out.println("Скобочная последовательность неверная");
//        }
    }

    private static int check(String example) {
        Stack stack = new Stack();
        for (char ch : example.toCharArray()) {
            if (ch == '(' || ch == '{' || ch == '[') {
                stack.put(ch);

            //  если скобка закрывающаяся
            } else {
                if (stack.getSize() == 0) {
                    return -1;
                }
                if ((stack.pop() == '(' && ch == ')')
                    || (stack.pop() == '[' && ch == ']')
                    || (stack.pop() == '{' && ch == '}')) {
                    stack.delete();
                }
            }
        }
        return stack.getSize();
    }
}
