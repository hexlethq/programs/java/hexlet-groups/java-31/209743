// BEGIN
package exercise;


import exercise.geometry.Point;
import exercise.geometry.Segment;

// END
public class App {
    public static double[] getMidpointOfSegment(double[][] segment){
        double[] pointBeg = Segment.getBeginPoint(segment);
        double[] pointEnd = Segment.getEndPoint(segment);
        double xMid = (Point.getX(pointBeg) + Point.getX(pointEnd)) /2;
        double yMid = (Point.getY(pointBeg) + Point.getY(pointEnd)) /2;
        return Point.makePoint(xMid,yMid);
    }
    public static double[][] reverse(double[][] segment) {
        double[] pointBeg = Segment.getBeginPoint(segment);
        double[] pointEnd = Segment.getEndPoint(segment);
        double[] newX = Point.makePoint(Point.getX(pointEnd),Point.getY(pointEnd));
        double[] newY = Point.makePoint(Point.getX(pointBeg),Point.getY(pointBeg));
        return Segment.makeSegment(newX,newY);
    }
    public static boolean isBelongToOneQuadrant(double[][] segment) {
        double[] pointBeg = Segment.getBeginPoint(segment);
        double[] pointEnd = Segment.getEndPoint(segment);
        if (getQuadrant(pointBeg) ==getQuadrant(pointEnd)) {
            return  true;
        }
        return  false;
    }
    public static int getQuadrant(double[] point){
        double x = Point.getX(point);
        double y = Point.getY(point);
        if (x > 0 && y > 0) {
            return  1;
        }
        if (x < 0 && y > 0) {
            return  2;
        }
        if (x < 0 && y < 0) {
            return  3;
        }
        if (x > 0 && y < 0) {
            return  4;
        }
        return  0;
    }
}