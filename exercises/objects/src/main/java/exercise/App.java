package exercise;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

class App {
    // BEGIN
    public static void main(String[] args) throws Exception {
        String[] animals = {"cats", "dogs", "birds"};
        System.out.println(App.buildList(animals));

        String[][] users = {
                {"Andrey Petrov", "1990-11-23"},
                {"Aleksey Ivanov", "1995-02-15"},
                {"John Smith", "1990-03-11"},
                {"Vanessa Vulf", "1985-11-16"},
                {"Vladimir Nikolaev", "1990-12-27"},
                {"Alice Lucas", "1986-01-01"},
        };
        System.out.println(App.getUsersByYear(users, 1990));

        String[][] users1 = {
                {"Andrey Petrov", "1990-11-23"},
                {"Aleksey Ivanov", "2000-02-15"},
                {"Anna Sidorova", "1996-09-09"},
                {"John Smith", "1989-03-11"},
                {"Vanessa Vulf", "1985-11-16"},
                {"Vladimir Nikolaev", "1990-12-27"},
                {"Alice Lucas", "1986-01-01"},
                {"Elsa Oscar", "1989-03-10"},
        };
        System.out.println(App.getYoungestUser(users1, "11 Jul 1989")); // "John Smith"
    }

    public static  String buildList(String[] arayString){
        StringBuilder strbuld = new StringBuilder();
        if (arayString.length > 1){
            strbuld.append("<ul>\n");
            for (String word:arayString) {
                strbuld.append(writeNameUserHTML(word));
            }
            strbuld.append("</ul>");
            return  strbuld.toString();
        }
        return "";
    }

    public static  String writeNameUserHTML(String username){
        StringBuilder strbuld = new StringBuilder();
        strbuld.append("  <li>");
        strbuld.append(username);
        strbuld.append("</li>\n");
        return  strbuld.toString();
    }

    public static String getUsersByYear(String[][] orgDoubleArray, int year) {
        StringBuilder strbuld = new StringBuilder();
        boolean doatleast = false;
        if (orgDoubleArray.length > 1) {
            strbuld.append("<ul>\n");
            for (String[] oneArray : orgDoubleArray) {
                String stringYear = oneArray[1].substring(0, 4);
                if (Integer.parseInt(stringYear) == year) {
                    strbuld.append(writeNameUserHTML(oneArray[0]));
                    doatleast = true;
                }
            }
            strbuld.append("</ul>");
            if (doatleast) {
                return strbuld.toString();
            } else {
                return "";
            }
        }
        return "";
    }
    // END

    // Это дополнительная задача, которая выполняется по желанию.
    public static String getYoungestUser(String[][] users, String date) throws Exception {
        // BEGIN
        SimpleDateFormat formatterin = new SimpleDateFormat("dd MMM yyyy", Locale.US);
        Date dateInObj = formatterin.parse(date);

        Date dateTemp = new Date(50000000L);
        String stringTemp = "";
        SimpleDateFormat formatterForLoop = new SimpleDateFormat("yyyy-MM-dd");
        for (String[] oneArray : users) {
            Date dateLoopObj= formatterForLoop.parse(oneArray[1]);
            if (dateLoopObj.before(dateInObj) && dateLoopObj.after(dateTemp)) {
                dateTemp = dateLoopObj;
                stringTemp = oneArray[0];
            }

        }
        return stringTemp;
        // END
    }
}
