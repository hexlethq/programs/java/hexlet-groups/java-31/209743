package exercise;

import java.util.Arrays;

class App {
    // BEGIN
    public static void main(String[] args){
        int[] numbers = {1, 2, -6, 3, 8};
        int[] result = App.reverse(numbers);
        System.out.println(Arrays.toString(result));
        int[] numbers1 = new int[0];
        int[] result1 = App.reverse(numbers1);
        System.out.println(Arrays.toString(result1));
        int[] numbers2 = {1, 4, 3, 4, 5};
        System.out.println(App.mult(numbers2)); // 240

        int[][] matrix1 = new int[0][0];
        int[] result11 = App.flattenMatrix(matrix1);
        System.out.println(Arrays.toString(result11));

        int[][] matrix2 = {
                {1, 2, 3},
                {4, 5, 6}
        };
        int[] result2 = App.flattenMatrix(matrix2);
        System.out.println(Arrays.toString(result2));
    }
    public static int[] reverse(int [] origArray){
        int[] returnArray = new int[origArray.length];
        if (origArray.length > 0 ) {

            int j = 0;
            for (int i = origArray.length - 1; i >= 0; i--, j++) {
                returnArray[j] = origArray[i];
            }
        }
        return returnArray;
    }
    public static int mult(int [] origArray) {
        int returnVal = 1;
        for (int i = origArray.length - 1; i >= 0; i--){
            returnVal = returnVal*origArray[i];
        }
        return returnVal;
    }

    public static int[] flattenMatrix(int[][] orgMatrix) {
        int collumNum = orgMatrix.length;
        if (collumNum > 1) {
            int StringNum = orgMatrix[0].length;
            int[] returnArray = new int[collumNum * StringNum];
            int d = 0;

            for (int i = 0; i < collumNum; i++) {
                for (int j = 0; j < StringNum; j++) {
                    returnArray[d] = orgMatrix[i][j];
                    d++;
                }

            }
            return returnArray;
        } else {
            return new int[0];
        }
        // END
    }
}
